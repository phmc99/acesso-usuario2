export function WelcomePage({ user, setIsLoggedIn }) {
  function handleLogOut() {
    return setIsLoggedIn(false);
  }

  return (
    <div>
      <h1>
        Bem vindo, <span>{user}</span>!
      </h1>
      <button onClick={handleLogOut}>LogOut</button>
    </div>
  );
}

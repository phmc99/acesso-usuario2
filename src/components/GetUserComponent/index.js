import { useState } from "react";

export function GetUserComponent({ setUser, setIsLoggedIn }) {
  const [userInput, setUserInput] = useState("");

  function handleLogIn(user) {
    setUser(user);
    setIsLoggedIn(true);
    return;
  }

  return (
    <form action="">
      <input
        type="text"
        value={userInput}
        onChange={(event) => setUserInput(event.target.value)}
      />
      <button onClick={() => handleLogIn(userInput)}>LogIn</button>
    </form>
  );
}
